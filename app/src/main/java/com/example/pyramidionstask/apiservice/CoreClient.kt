package com.example.pyramidionstask.apiservice

import com.example.pyramidionstask.model.EmployeeList
import retrofit2.Call
import retrofit2.http.GET

interface CoreClient {

    @GET("employees")
    fun coreDetails(): Call<EmployeeList?>?
}