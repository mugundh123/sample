package com.example.pyramidionstask.model

import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

class EmployeeList {


    var data: List<Employee> =
        ArrayList<Employee>()

    @SerializedName("status")
    var status: String? = null

    class Employee {
        var id: String? = null
        var employee_name: String? = null
        var employee_salary: String? = null
        var employee_age: String? = null
        var profile_image: String? = null

    }

}