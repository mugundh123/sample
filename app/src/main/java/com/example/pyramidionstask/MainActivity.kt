package com.example.pyramidionstask

import android.app.Dialog
import android.app.SearchManager
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.MenuItemCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pyramidionstask.adapter.EmployeeListAdapter
import com.example.pyramidionstask.apiservice.CoreClient
import com.example.pyramidionstask.model.EmployeeList
import com.example.yoyo.apiService.ServiceGenerator
import com.squareup.picasso.Picasso
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
lateinit var employeeListView:RecyclerView
    lateinit var noData:TextView
    lateinit var getData:TextView
    lateinit var  mDialog: Dialog
    lateinit var employeeListAdapter:EmployeeListAdapter
    lateinit var searchView: androidx.appcompat.widget.SearchView
    var isAsc:Boolean=true
    var listEmployee:ArrayList<EmployeeList.Employee> = ArrayList<EmployeeList.Employee>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        employeeListView=findViewById<RecyclerView>(R.id.employee_list)
        employeeListView.layoutManager=LinearLayoutManager(this)
        noData=findViewById(R.id.nodataTxt)


    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_item, menu)
        var searchItem: MenuItem = menu.findItem(R.id.action_search);
        var sort:MenuItem=menu.findItem(R.id.ascending)
        sort.setOnMenuItemClickListener {
            sortData(isAsc)
            isAsc = if(isAsc){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    sort.icon=getDrawable(R.drawable.ic_arrow_drop_up_black_24dp)
                }

                false
            } else{
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    sort.icon=getDrawable(R.drawable.ic_arrow_drop_down_black_24dp)
                }
                true
            }

            true
        }
        // Retrieve the SearchView and plug it into SearchManager
        searchView = MenuItemCompat.getActionView(searchItem) as SearchView

        val searchManager =
            getSystemService(Context.SEARCH_SERVICE) as SearchManager

        val searchPlate: EditText =
            searchView.findViewById<View>(androidx.appcompat.R.id.search_src_text) as EditText
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchPlate.setHint("Search")
        val searchPlateView =
            searchView.findViewById<View>(androidx.appcompat.R.id.search_plate)
        searchPlateView.setBackgroundColor(
            ContextCompat.getColor(
                this,
                android.R.color.transparent
            )
        )
        searchView.setOnCloseListener {
            onResume()
             true
        }
        searchView.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                // use this method when query submitted
                filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                // use this method for auto complete search process
                filter(newText)
                return false
            }
        })
        searchView.setOnSearchClickListener {
            //some operation

        }


        return true
    }
    override fun onBackPressed() {
        if (!searchView.isIconified) {
            searchView.isIconified = true
            supportActionBar?.title=""
        } else {
            super.onBackPressed()
        }
    }
    override fun onResume() {
        super.onResume()
        if(isNetworkAvailable(this)) {
            listEmployee.clear()
            callEmployeeData()
        }
        else
        {
            Toast.makeText(this@MainActivity,"Internet connection Required",Toast.LENGTH_LONG).show()
        }
    }
    private fun callEmployeeData() {
        
        val client: CoreClient =
            ServiceGenerator(this).createService(CoreClient::class.java)
        
        val coreResponse: Call<EmployeeList?>? = client.coreDetails()
        showDialog()
        coreResponse!!.enqueue(
                object : Callback<EmployeeList?> {
                    override fun onResponse(
                        call: Call<EmployeeList?>,
                        response: Response<EmployeeList?>
                    ) {
                        closeDialog()
                        if (response.isSuccessful) {
                            val employeeList: EmployeeList? = response.body()
                            if (employeeList != null) {
                                if (!employeeList.status.isNullOrBlank()) {
                                    println("aaaa==="+ employeeList.data.size)
                                    listEmployee.addAll(employeeList.data)
                                    employeeListAdapter=EmployeeListAdapter(
                                        this@MainActivity,
                                        listEmployee
                                    )
                                    employeeListView.adapter = employeeListAdapter
                                    if (listEmployee.size === 0)
                                        noData.visibility = View.VISIBLE
                                    else
                                        noData.visibility = View.GONE
                                } else {
                                    Toast.makeText(this@MainActivity,"Error occured1",Toast.LENGTH_LONG).show()
                                }
                            } else {

                                Toast.makeText(this@MainActivity,"Error occured2",Toast.LENGTH_LONG).show()
                            }
                        } else {

                            Toast.makeText(this@MainActivity,"Error occured3",Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onFailure(
                        call: Call<EmployeeList?>,
                        t: Throwable
                    ) {
                        t.printStackTrace()
//                        closeDialog()
                        Toast.makeText(this@MainActivity,"Server Error",Toast.LENGTH_LONG).show()

                        // (this != null)
                        //((MainActivity) this).closeProgressDialog();
                    }
                })

    }
    fun filter(text: String?) {
        val temp: ArrayList<EmployeeList.Employee> = ArrayList()
        for (d in listEmployee) {
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if (d.employee_name?.toLowerCase()!!.contains(text.toString().toLowerCase())) {
                temp.add(d)
            }
        }
        //update recyclerview
        employeeListAdapter.updateList(temp)
    }
    fun showDialog() {
        try {

                val view =
                    View.inflate(this, R.layout.progress_bar, null)
                mDialog = Dialog(this, R.style.dialogwinddow)
                mDialog.setContentView(view)
                mDialog.setCancelable(false)
                mDialog.show()
                val iv =
                    mDialog.findViewById(R.id.giff) as ImageView
                Picasso.get().load(R.drawable.loading_anim)
                    .into(iv)

        } catch (e: Exception) {
        }
    }

    fun closeDialog() {
        try {
            if (mDialog != null) if (mDialog.isShowing) mDialog.dismiss()
        } catch (e: Exception) {
        }
    }
    fun isNetworkAvailable(context: Context?): Boolean {
        if (context == null) return false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if (capabilities != null) {
                    if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        return true
                    } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                        return true
                    }
                }
            } else {
                try {
                    val activeNetworkInfo = connectivityManager.activeNetworkInfo
                    if (activeNetworkInfo != null && activeNetworkInfo.isConnected) {
                        Log.i("update_statut", "Network is available : true")
                        return true
                    }
                } catch (e: java.lang.Exception) {
                    Log.i("update_statut", "" + e.message)
                }
            }
        }
        Log.i("update_statut", "Network is available : FALSE ")
        return false
    }
    private fun sortData(asc: Boolean) {
        //SORT ARRAY ASCENDING AND DESCENDING
        if (asc) {
            Collections.sort(listEmployee,
                Comparator<EmployeeList.Employee> { lhs, rhs -> lhs.employee_name!!.compareTo(rhs.employee_name.toString()) })
        } else {
            Collections.reverse(listEmployee)
        }
        employeeListAdapter.updateList(listEmployee)
    }
}
