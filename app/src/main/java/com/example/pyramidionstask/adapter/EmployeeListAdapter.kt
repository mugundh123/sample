package com.example.pyramidionstask.adapter

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.pyramidionstask.R
import com.example.pyramidionstask.adapter.EmployeeListAdapter.CustomViewHolder
import com.example.pyramidionstask.model.EmployeeList
import com.squareup.picasso.Picasso

class EmployeeListAdapter(
    private val mContext: Context,
    private var data: ArrayList<EmployeeList.Employee>
) :
    RecyclerView.Adapter<CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val inflater = LayoutInflater.from(mContext)
        var view: View? = null
        view = inflater.inflate(R.layout.employee_list_item, parent, false)
        return CustomViewHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        if (data[position].profile_image!!.trim { it <= ' ' } != "") {
            Picasso.get().load(data[position].profile_image).into(holder.empImg)
        }
        else{
            println("11111111")
            holder.empImg.setImageDrawable(mContext.getDrawable(R.drawable.loadingimage))
        }
        holder.empName.text = "Employee Name: "+data[position].employee_name
        holder.empSalary.text = "Employee Salary: "+data[position].employee_salary
        holder.empAge.text = "Employee Age: "+data[position].employee_age
    }

    override fun getItemCount(): Int {
        return data.size
    }

    inner class CustomViewHolder(v: View?) : ViewHolder(v!!) {
        var empImg: ImageView
        var empName: TextView
        var empSalary: TextView
        var empAge: TextView

        init {
            empImg = v!!.findViewById<View>(R.id.emp_img) as ImageView
            empName = v.findViewById<View>(R.id.emp_name) as TextView
            empSalary = v.findViewById<View>(R.id.emp_salary) as TextView
            empAge = v.findViewById<View>(R.id.emp_age) as TextView
        }
    }

    fun updateList(list: List<EmployeeList.Employee?>) {
        data = list as ArrayList<EmployeeList.Employee>
        notifyDataSetChanged()
    }

}